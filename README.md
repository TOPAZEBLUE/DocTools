JuPyTools
================================

Python and Jupyter tools

## Requirements

This repo requires [JupyText][jupytext] to reconstitute the Jupyter Notebooks.

[jupytext]:https://github.com/mwouts/jupytext
