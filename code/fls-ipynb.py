# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.13.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

import os
from fls import *

# # FLS Demo Notebook

if not 'SCRIPTPATH' in globals():
    SCRIPTPATH = os.getcwd()
print('SCRIPTPATH', SCRIPTPATH)
os.chdir(SCRIPTPATH) 

# ## FSave

help(fsave)

TEXT = """
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus a quam metus.
Donec faucibus, leo nec pharetra congue, enim libero consequat eros, ut
condimentum ligula justo sit amet velit. Quisque ut massa non lectus scelerisque
facilisis vitae id erat. Orci varius natoque penatibus et magnis dis parturient
montes, nascetur ridiculus mus. Aenean vel elementum mi, vel porttitor enim.
Aenean pharetra sed quam ac varius. Etiam in tortor ac enim tincidunt dapibus
eget sit amet nisl. Cras augue tortor, malesuada non leo ut, ullamcorper
hendrerit ligula. Cras ultrices nunc auctor nisi pulvinar, luctus efficitur dui
elementum. Proin at est ligula. Proin et erat ac massa dapibus pharetra non a
nisi.
"""

fsave(TEXT, "fls-demo.txt")

# !ls

# # FLoad

help(fload)

text = fload("fls-demo.txt")
print(text)


